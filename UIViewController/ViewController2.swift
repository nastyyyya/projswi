//
//  ViewController2.swift
//  vdgs
//
//  Created by Baranova Anastasiya Aleksandrovna on 22.03.2023.
//

import UIKit

class ViewController2: UIViewController {
    @IBOutlet weak var button: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    deinit {
        print ("Deallocated")
    }
    @IBAction func didPressedBack (_backButton: UIButton) {
        dismiss (animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
