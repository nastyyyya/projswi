//
//  ViewController3.swift
//  vdgs
//
//  Created by Baranova Anastasiya Aleksandrovna on 22.03.2023.
//

import Foundation
import UIKit
class ViewController3: UIViewController {
    @IBOutlet weak var button: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func didPressedBack (_backButton: UIButton) {
        navigationController?
            .popViewController(animated: true)
    }
}
