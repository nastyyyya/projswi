//
//  TableViewExtension.swift
//  TableView
//
//  Created by Baranova Anastasiya Aleksandrovna on 22.03.2023.
//

import Foundation
import UIKit

extension UITableView {
    func registerCustomCell (_cell: CustomCell.Type)
    {
        self.register(nib:cellNib, forCellReuseIdentifier: cell.cellIdentifier())
    }
}
