//
//  ViewController.swift
//  TableView
//
//  Created by Baranova Anastasiya Aleksandrovna on 22.03.2023.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    
    
    

    @IBOutlet weak var tableView: UITableView!
    var users: [UserModel] = []
   
    override func viewDidLoad() {
        super.viewDidLoad()
            
            users.append (UserModel (tittle: "Anton", age: "10000"))
            users.append (UserModel (tittle: "Vanya", age: "18"))
            users.append (UserModel (tittle: "Andrey", age: "18"))
            users.append (UserModel (tittle: "Nikita", age: "18"))
            users.append (UserModel (tittle: "Varya", age: "18"))
            users.append (UserModel (tittle: "Maria", age: "18"))
        tableView.delegate = self
        tableView.dataSource = self
        
        
        tableView.registerCustomCell(CustomTableViewCell.self)
    
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return users.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: indexPath) -> UITableViewCell
    {
        let cell =
        tableView.dequeueReusableCell(withIdentifier: CustomTableViewCell.cellIdentifier(), for: indexPath) as! CustomTableViewCell
        
        let model = users [indexPath.row]
        cell. configure (with: model)
        return cell
    }
}
extension UIViewController {
    var myId: String {
        get {
            return ""
        }
    }
    func sayHello (){
    }
}
