//
//  ViewController.swift
//  TableView
//
//  Created by Baranova Anastasiya Aleksandrovna on 22.03.2023.
//

import UIKit

protocol ViewControllerDelegate: AnyObject {
    func didChangeUser (_user: UserModel)
    
}
enum Names: String {
    case Anton = "Anton"
    case Vanya = "Vanya"
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CustomCellDelegate, ViewControllerDelegate {
    
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
    var users: [UserModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        users.append (UserModel (tittle: Names.anton.rawValue, age: "10000"))
        users.append (UserModel (tittle: "Vanya", age: "18"))
        tableView.delegate = self
        tableView.dataSource = self
        
        
        tableView.registerCustomCell(CustomTableViewCell.self)
        
    }
    func didPressAction(for cell: UITableViewCell) {
        guard let indexPath = tableView.indexPath (for:cell)else {
            return
            
        }
            let model = users [indexPath.row]
            performSegue (withIdentifier: "detailIdentifier", sender: model)
    }
    func didChangeUser (_user:UserModel)
    {
        if let userValue = users.enumerated().first(where: {$0.element.tittle == user.tittle})
        {
            users.remove (at: userValue.offset)
            users.insert (user, at:userValue.offset)
            
            tableView.reloadData()
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return users.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: indexPath) -> UITableViewCell
    {
        let cell =
        tableView.dequeueReusableCell(withIdentifier: CustomTableViewCell.cellIdentifier(), for: indexPath) as! CustomTableViewCell
        
        
        let model = users [indexPath.row]
        cell. configure (with: model, delegate: self)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowat indexPath: IndexPath){
        
        //tableView.deselectRow(at: IndexPath, animated: true)
        
        //let model = users [indexPath.row]
        //performSegue (withIdentifier: "detailIdentifier", sender: model)
        
    }
    
    override func prepare (for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "detailIdentifier", let userModel = sender as? UserModel
        {
            let destController = seque.destination as!
            DetailViewController
            destController.user=userModel
            destController.delegate = self
        }
    }
}
