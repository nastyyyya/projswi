//
//  DetailViewController.swift
//  TableView
//
//  Created by Baranova Anastasiya Aleksandrovna on 22.03.2023.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet private weak var nameLabel: UILabel: UILabel!
    @IBOutlet private weak var ageLabel: UILabel: UILabel!
    
    var user: UserModel!
    weak var delegate: ViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameLabel.text = user.tittle
        ageLabel.text = user.age
        
        nameLabel.sizeToFit()
        ageLabel.sizeToFit()
        changeData()
    }
    
private func changeData()
    {
        guard let ageInt = Int (user.age) else {return}
        user.age = String (ageInt + 22)
        delegate?.didChangeUser(_user: )
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
