//
//  CustomTableViewCell.swift
//  TableView
//
//  Created by Baranova Anastasiya Aleksandrovna on 22.03.2023.
//

import UIKit
protocol CustomCell
{
        static func cellNib () -> UINib
        
        static func cellIdentifier() -> String
     
}

protocol CustomCellDelegate: AnyObject {
    func didPressAction (for cell:UITableViewCell)
}

class CustomTableViewCell: UITableViewCell, CustomCell {

    @IBOutlet weak var labelOne: UILabel!
    @IBOutlet weak var labelTwo: UILabel!
    
    var delegate: CustomCellDelegate?
    
    func configure (with user: UserModel, delegate:CustomCellDelegate)
    {
        labelOne.text = user.age
        labelTwo.text = user.tittle
        self.delegate = delegate
    }
    static func cellNib () -> UINib {
        return UINib (nibName: String, bundle: describing; self, bundle: nil)
    }
    
    static func cellIdentifier() -> String{
        return String (describing: self)
    }
    @IBAction func
    buttonAction(_sender: Any)
    {
        delegate?.didPressAction(for: self)
    }
}


