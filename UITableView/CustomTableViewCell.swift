//
//  CustomTableViewCell.swift
//  TableView
//
//  Created by Baranova Anastasiya Aleksandrovna on 22.03.2023.
//

import UIKit
protocol CustomCell
{
        static func cellNib () -> UINib
        
        static func cellIdentifier() -> String
     
}

class CustomTableViewCell: UITableViewCell, CustomCell {

    @IBOutlet weak var
    labelOne:
    UILabel!
    @IBOutlet weak var
    labelTwo:
    UILabel!
    
    func configure (with user: UserModel)
    {
        labelOne.text = user.age
        labelTwo.text = user.tittle
    }
    static func cellNib () -> UINib {
        return UINib (nibName: String, bundle: describing; self, bundle: nil)
    }
    
    static func cellIdentifier() -> String{
        return String (describing: self)
    }
}


