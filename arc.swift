struct passengerCar {
    let carBrand: String
    let yearOfIssue: Int
    let trunkVolume: Float
    var isEngineStarted: Bool
    var isWindowsOpen: Bool
    var trunkSpace: Float

    mutating func closeOrOpenWindow(the windowOperation: WindowOperations) {
        switch windowOperation {
        case .open:
            if self.isWindowsOpen {
                print("Окно уже открыто")
            } else {
                self.isWindowsOpen = true
                print("Окно открылось")
            }
        case .close:
            if !self.isWindowsOpen {
                print("Окно уже закрыто")
            } else {
                self.isWindowsOpen = false
                print("Окно закрылось")
            }
        }
    }

    mutating func switchOnOffEngine (the engineOpertion: EngineOperations) {
        switch engineOpertion {
        case .on:
            if isEngineStarted {
                print("Двигатель уже заведен")
            } else {
                self.isEngineStarted = true
                print("Двигатель заведен")
            }
        case .off:
            if !isEngineStarted {
                print("Двигатель уже выключен")
            } else {
                self.isEngineStarted = false
                print("Двигатель выключен")
            }
        }
    }
}

struct cargoCar {
    let carBrand: String
    let yearOfIssue: Int
    let bodyVolume: Float
    var isEngineStarted: Bool
    var isWindowsOpen: Bool
    var bodySpace: Float
}

var bugattiChiron = passengerCar(carBrand: "BMW", yearOfIssue: 2020, trunkVolume: 10.0, isEngineStarted: false, isWindowsOpen: false, trunkSpace: 3.0)
var tatra148 = cargoCar(carBrand: "Kamaz", yearOfIssue: 2019, bodyVolume: 100.0, isEngineStarted: true, isWindowsOpen: true, bodySpace: 100.0)


enum EngineOperations {
    case on
    case off
}

enum WindowOperations {
    case open
    case close
}

enum TrunkOperations {
    case download
    case upload
}



bugattiChiron.closeOrOpenWindow(the: WindowOperations.open)
print(bugattiChiron.isWindowsOpen)



// Capture list могут захватывать и хранить ссылки на любые константы и переменные из контекста, в котором они определены
//По умолчанию используется strong (сильный захват)
//Чтобы использовать слабый захват (weak) - нужно указать [weak ...].
//Чтобы использовать захват без владельца (unowned) - нужно указать [unowned ...]. Позволяет значениям стать равными нулю в любой момент в будущем


class Dancer {
    func playSong() {
        print("dancedancedance")
    }
}
func dance() -> () -> Void {
    let taylor = Dancer()

    let dancing = {
        taylor.playSong()
        return
    }

    return dancing
}
let dancFunction = dance()
dancFunction()

class Car {
  var driver: Man?
  deinit {//выведем сообщение в консоль о том, что объект удален}
  print ("Машина удалена из памяти")
}
}
class Man {
 weak var myCar: Car?
  deinit {//выведем в консоль сообщение о том, что объект удален}
  print ("Мужчина удален из памяти")
}
}
var car: Car? = Car()
var man: Man? = Man()
car?.driver = man
man?.myCar = car
car = nil
man = nil



class Man {
    var passport: Passport?

    deinit {//выведем в консоль сообщение о том, что объект удален
        print("Мужчина удален из памяти")
    }
}

class Passport {
    unowned var man: Man

    init(man: Man) {
        self.man = man
    }

    deinit {
        print("Паспорт удален из памяти")
    }
}

var man: Man? = Man()
var passport: Passport? = Passport(man: man!)
man?.passport = passport
passport = nil //объект еще не удален, его удерживает мужчина
man = nil //теперь удалены оба объекта