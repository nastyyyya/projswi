struct IOSCollection {
    var id = 1
}

class Ref<T> {
    var value: T
    init(value: T) {
        self.value = value
    }
}

struct Container<T> {
    var ref: Ref<T>
    init(value: T) {
        self.ref = Ref(value: value)
    }
    var value: T {
        get {
         ref.value
        }
        set {
            guard isKnownUniquelyReferenced(&ref) else {
                ref = Ref(value: newValue)
                return
            }
            ref.value = newValue
        }
    }
}
var id = IOSCollection()
var container1 = Container(value:id)
var container2 = container1
container2.value.id = 12


protocol GameDice {
    var numberDice: Int {get}
}
extension Int {
    var numberDice: Any{
         print("Выпало \(self) на кубике")
    }
}

let diceCoub = 5
diceCoub.numberDice



protocol Hotel {
    init(roomCount: Int)
}

class HotelAlfa: Hotel {
    var roomCount: Int = 0

    required init(roomCount: Int) {
        self.roomCount = roomCount
    }
}
var hotel = HotelAlfa (roomCount:200)



protocol Start {
    var time: String {get set}
    var strokesAmount: Int {get set}

    func writeCode()
}


protocol End {
    func stopCoding()
}

typealias Workable = Start & End

class Developer: Workable {

    enum Platforms: String {
        case iOS
        case android = "Android"
        case web = "Web"
    }

    var time: String
    var strokesAmount: Int
    var platform: Platforms
    var numberOfSpecialist: Int

    init(platform: Platforms, developers: Int, time: String, strokes: Int) {
        self.numberOfSpecialist = developers
        self.platform = platform
        self.strokesAmount = strokes
        self.time = time
    }

    func writeCode() {
        print("Разработка началась. Пишем код. \(strokesAmount) строк за \(time).")
    }

    func stopCoding() {
        print("Работа закончена. Сдаю в тестирование. Код писало \(numberOfSpecialist) человек.")
    }
}


let developer = Developer(platform: .android, developers: 100, time: "18 часов", strokes: 1000000000000)
developer.writeCode()
developer.stopCoding()