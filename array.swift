//Создать массив элементов 'кол-во дней в месяцах' содержащих количество дней в соответствующем месяце
var days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ] 
 //Создать массив элементов 'название месяцов' содержащий названия месяцев
let month =  ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November","December"]
//Используя цикл for и массив 'кол-во дней в месяцах' выведите количество дней в каждом месяце (без имен месяцев)
for day in days {print (day)}
// Используйте еще один массив с именами месяцев, чтобы вывести название месяца + количество дней
for i in 0...11 {print ("name \(month[i]) - quantity \(days[i])")}
//Сделайте тоже самое, но используя массив tuples (кортежей) с параметрами (имя месяца, кол-во дней)
var turtles: [(String,Int)] = []
for i in 0...11
{var turtle = (month[i], days[i])
turtles.append(turtle)
}
print (turtles)
//Сделайте тоже самое, только выводите дни в обратном порядке (порядок в массиве не менять)
var reserve: [(String,Int)] = []
for i in 0...11
{var turtle = (month[i], days[11-i])
reserve.append(turtle)
}
print (reserve)