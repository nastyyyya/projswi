//
//  PhotoCollectionViewCell.swift
//  CollectionViews
//
//  Created by Baranova Anastasiya Aleksandrovna on 22.03.2023.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    static let identifier = "PhotoCollectionViewCell"
    
        private let imageView: UIImageView = {
            let imageView = UIImageView()
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            return imageView
          }()
    
    
    override init(frame:CGRect) {
        super.init(frame: frame)
        contentView.addSubview(imageView)
        
        let images = [
        UIImage(named: "image"),
        UIImage(named: "image 1"),
        UIImage(named: "image 2"),
        ].compactMap({ $0 })
        imageView.image = images.randomElement()
    }
    required init? (coder: NSCoder)
    {
        fatalError()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = contentView.bounds
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }
}
